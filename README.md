# Allsaints Test

A simple restful golang API for Allsaints



## Getting Started

### Dependencies

* Docker compose

### Installing

* Simply run docker-compose up --build in the main directory

### Executing program

* Once docker compose has completed the program should be ready to use.
* You can access the API from using localhost on port 80
* There are several postman tests saved to allow for easy testing.
