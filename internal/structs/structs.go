package structs

import (
	"context"
	"time"
)

type Orders struct {
	Orders []Order
}

type Order struct {
	OrderID    int64     `json:"order_id" db:"order_id"`
	OrderDate  time.Time `json:"order_date" db:"order_date"`
	OrderValue float64   `json:"order_value" db:"order_value"`
	Status     string    `json:"status" db:"status"`
	StoreID    int64     `json:"store_id" db:"store_id"`
	Store      Store
}
type Store struct {
	StoreID int64  `json:"store_id" db:"store_id"`
	Name    string `json:"store_name" db:"store_name"`
}
type DateRange struct {
	MinDate time.Time `json:"min-date"`
	MaxDate time.Time `json:"max-date"`
}

type OrderStore interface {
	GetAllOrders(ctx context.Context) ([]Order, error)
	GetOrderByStatus(ctx context.Context, status string) ([]Order, error)
	CreateOrder(ctx context.Context, order Order) (Order, error)
	UpdateOrder(ctx context.Context, order Order) error
	DeleteOrder(ctx context.Context, order Order) error
}
