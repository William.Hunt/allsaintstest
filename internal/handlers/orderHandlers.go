package handlers

import (
	"AllSaintsTest/internal/logic"
	"AllSaintsTest/internal/structs"
	"AllSaintsTest/pkg/database"
	"AllSaintsTest/pkg/router"
	"encoding/json"
	"fmt"
	"net/http"
)

func init() {
	router.AddDef(router.Route{Method: "GET", Pattern: "/Orders", HandlerFunc: GetAllOrders})
	router.AddDef(router.Route{Method: "GET", Pattern: "/Orders/{status}", HandlerFunc: GetOrdersByStatus})
	router.AddDef(router.Route{Method: "GET", Pattern: "/Revenue", HandlerFunc: GetTotalRevenue})
	router.AddDef(router.Route{Method: "POST", Pattern: "/CreateOrder", HandlerFunc: CreateOrder})
	router.AddDef(router.Route{Method: "DELETE", Pattern: "/CancelOrder", HandlerFunc: CancelOrder})
}

func GetAllOrders(w http.ResponseWriter, r *http.Request) {
	jsonString, err := json.Marshal(logic.GetAllOrders())
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))
}
func GetOrdersByStatus(w http.ResponseWriter, r *http.Request) {
	data := database.GetParameters(r)
	jsonString, err := json.Marshal(logic.GetOrderByStatus(data["status"]))
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))
}
func GetTotalRevenue(w http.ResponseWriter, r *http.Request) {
	var dateRange structs.DateRange
	err := json.NewDecoder(r.Body).Decode(&dateRange)
	jsonString, err := json.Marshal(logic.GetTotalRevenue(dateRange))
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))
}
func CreateOrder(w http.ResponseWriter, r *http.Request) {
	var order structs.Order
	err := json.NewDecoder(r.Body).Decode(&order)
	jsonString, err := json.Marshal(logic.CreateOrder(order))
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))
}
func CancelOrder(w http.ResponseWriter, r *http.Request) {
	var order structs.Order
	err := json.NewDecoder(r.Body).Decode(&order)
	jsonString, err := json.Marshal(logic.CancelOrder(order))
	fmt.Println(err)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, string(jsonString))
}
