package logic

import (
	"AllSaintsTest/internal/structs"
	"AllSaintsTest/pkg/database"
	"database/sql"
	"fmt"
)

func GetAllOrders() []map[string]interface{} {
	return database.GetQueryAsArray(`select * from tbl_orders`, database.MySql)
}
func GetOrderByStatus(status string) []map[string]interface{} {
	return database.GetQueryAsArray(`select * from tbl_orders where status = ?`, database.MySql, status)
}

func GetTotalRevenue(dateRange structs.DateRange) []map[string]interface{} {
	return database.GetQueryAsArray(`select sum(order_value) from tbl_orders where order_date > ? and order_date < ?`, database.MySql, dateRange.MinDate, dateRange.MaxDate)
}

func CreateOrder(order structs.Order) error {
	tx := database.GetTransaction(database.MySql)
	defer tx.Rollback()
	fmt.Println(order)
	database.RunDataChange(`insert into tbl_orders (order_date, order_value, status, store_id)
values (?,?,?,?)`, tx, order.OrderDate, order.OrderValue, order.Status, order.StoreID)
	tx.Commit()
	return nil
}
func CancelOrder(order structs.Order) error {
	var tx *sql.Tx
	var err error
	if tx, err = database.MySql.Connection.Begin(); err != nil {
		return err
	}
	defer tx.Rollback()
	database.RunDataChange(`update tbl_orders set status='cancelled' where order_id = ?`, tx, order.OrderID)
	tx.Commit()
	return nil
}
