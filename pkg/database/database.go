package database

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"net/http"
	"net/url"
	"strings"

	"runtime/debug"
)

type Key int

type JwtData struct {
	Username interface{} `json:"username"`
	jwt.StandardClaims
	Permissions []map[string]interface{}
}

type DataSource struct {
	Connection       *sql.DB
	Driver           string
	ConnectionString string
}

type ErrorResponse struct {
	Error       string
	StackTrace  string
	ErrorObject error
}

const JsonKey = "MyR9vknxF9hUHz8gnZf558SUKq9DnPSa"

const MyKey Key = 0

//this is where the database connections are declared
var MySql = &DataSource{Driver: "mysql", ConnectionString: "docker:docker@tcp(db:3306)/test_db"}

//this function initialised the connections when the server starts
func (source *DataSource) InitDB() error {
	var err error                                                             //Declare the err variable
	source.Connection, err = sql.Open(source.Driver, source.ConnectionString) //Attempt to open the database connection
	if err != nil {                                                           //Check if there is an error
		return err //If there is an error then return it
	}
	source.Connection.SetMaxIdleConns(10)           //Set the maximum idle connections to 10
	if err = source.Connection.Ping(); err != nil { //Test that the connection is working and check if it returns an error
		return err // Return the error if there is one
	}
	return nil //Return no error to show that it has succeeded
}

//this function initialised the connections when the server starts
func (source *DataSource) CheckDbConnection() error {
	if source.Connection == nil { // Check that the database connection has been initialised
		if err := source.InitDB(); err != nil { //try to reconnect if the connection isn't working.
			return err
		}
	}
	if err := source.Connection.Ping(); err != nil { // Check that the connection is working correctly
		if err := source.InitDB(); err != nil { //try to reconnect if the connection isn't working.
			return err
		}
	}
	return nil //Return no error to show that it has succeeded
}

//this function runs an insert/update/delete/etc statement to the passed data source
func RunDataChange(sqlCommand string, source *sql.Tx, values ...interface{}) sql.Result {
	stmt, err := source.Prepare(sqlCommand)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer stmt.Close()
	res, err := stmt.Exec(values...)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	return res
}

// This function connects to the provided database and returns the data
// as an array of key value pairs with the key as a string and the value
// as an interface ([]map[string]interface{})
func GetQueryAsArray(sqlCommand string, source *DataSource, params ...interface{}) []map[string]interface{} {
	if source.Connection == nil { // Check that the database connection has been initialised
		if err := source.InitDB(); err != nil { // try to initialise the connection if it hasn't been initialised and check to make sure it has worked
			panic(ErrorResponse{err.Error(), string(debug.Stack()), err}) // Throw an error if the connection failed
		}
	}
	if err := source.Connection.Ping(); err != nil { // Check that the connection is working correctly
		if err := source.InitDB(); err != nil { //try to reconnect if the connection isn't working.
			panic(ErrorResponse{err.Error(), string(debug.Stack()), err}) // Throw an error if the connection failed
		}
	}
	tx, err := source.Connection.Begin() // Initialise a transaction (tx)
	// This is probably not needed for a get request but it makes it easier to keep the code consistent.
	if err != nil { // check to make sure that the transaction worked
		panic(ErrorResponse{err.Error(), string(debug.Stack()), err}) // Throw an error if the transaction failed
	}
	defer tx.Rollback() // Defer the rollback until the function exits to make sure
	// that no changes are made if the function fails.
	stmt, err := tx.Prepare(sqlCommand) //pass the sql through to the transation which returns a prepared statement
	if err != nil {                     //check to make sure there is no error
		panic(ErrorResponse{err.Error(), string(debug.Stack()), err}) //throw the error if there is one
	}
	defer stmt.Close()                 //defer closing the stmt until the function finishes to make sure that we dont leave it open
	rows, err := stmt.Query(params...) //pass the params through to the statement and run the query
	// which this gives us a data stream called rows
	if err != nil { //check to make sure there is no error
		panic(ErrorResponse{err.Error(), string(debug.Stack()), err}) //throw the error if there is one
	}
	defer rows.Close()             //defer closing the rows stream until the function finishes to make sure that we dont leave the stream open
	columns, err := rows.Columns() //returns a array of all column headings
	if err != nil {                //check to make sure there is no error
		panic(ErrorResponse{err.Error(), string(debug.Stack()), err}) //throw the error if there is one
	}
	count := len(columns)                          //gets the length of the columns array
	tableData := make([]map[string]interface{}, 0) //initialses an array of key-value maps with a length of 0 to store the data
	values := make([]interface{}, count)           //initialses an array of interfaces the length of the total number of columns for storing individual values
	valuePtrs := make([]interface{}, count)        //initialses an array of interfaces the length of the total number of columns for storing value pointers
	for rows.Next() {                              // while rows.next return true do the following.
		// rows.next will cycle through all available rows and will then return false when there are no rows remaining
		for i := 0; i < count; i++ { //sets the value of valuePtrs to pointers to the values in the values array
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...) //this scans the data from the current row and using the pointers
		// stored in the valuePtrs array sets the values in the values array
		entry := make(map[string]interface{}) //creates a blank key value map
		for i, col := range columns {         //loops through each column in the columns array
			var v interface{}
			val := values[i]
			b, ok := val.([]byte) // tries to convert the current value to a byte array
			if ok {               //checks if the conversion succeeded
				v = string(b)
			} else {
				v = val
			}
			entry[col] = v //sets the current column key and adds the value to the map
		}
		tableData = append(tableData, entry) //adds the latest entry to the table data array
	}
	//stmt.Close()
	tx.Commit() //commits the transaction, again probably not needed for a get but required if using a transaction.
	return tableData
}

//this function runs a sql select statement to the passed data source and returns the response as a json array
func RunGet(sqlCommand string, source *DataSource, params ...interface{}) string {
	tableData := GetQueryAsArray(sqlCommand, source, params...)
	jsonData, err := json.Marshal(tableData)
	if err != nil {
		panic(ErrorResponse{err.Error(), string(debug.Stack()), err})
	}
	return string(jsonData)
}

func GetPostData(r *http.Request, database *DataSource) (tx *sql.Tx, jwtData JwtData, postData map[string]interface{}, params []interface{}) {
	jwtData, _ = r.Context().Value(MyKey).(JwtData)
	var err error
	if database.Connection == nil {
		if err = database.InitDB(); err != nil {
			panic(ErrorResponse{err.Error(), string(debug.Stack()), err})
		}
	}
	if err = database.Connection.Ping(); err != nil {
		if err = database.InitDB(); err != nil {
			panic(ErrorResponse{err.Error(), string(debug.Stack()), err})
		}
	}
	if tx, err = database.Connection.Begin(); err != nil {
		panic(ErrorResponse{err.Error(), string(debug.Stack()), err})
	}
	if err = json.NewDecoder(r.Body).Decode(&postData); err != nil {
		if err.Error() != "EOF" {
			panic(ErrorResponse{err.Error(), string(debug.Stack()), err})
		}
	}
	return
}
func GetTransaction(database *DataSource) (tx *sql.Tx) {
	var err error
	if database.Connection == nil {
		if err = database.InitDB(); err != nil {
			panic(ErrorResponse{err.Error(), string(debug.Stack()), err})
		}
	}
	if err = database.Connection.Ping(); err != nil {
		if err = database.InitDB(); err != nil {
			panic(ErrorResponse{err.Error(), string(debug.Stack()), err})
		}
	}
	if tx, err = database.Connection.Begin(); err != nil {
		panic(ErrorResponse{err.Error(), string(debug.Stack()), err})
	}
	return
}

func GetParameters(r *http.Request) (data map[string]string) {
	data = mux.Vars(r)
	for k := range data {
		data[k], _ = url.QueryUnescape(data[k])
		data[k] = strings.Replace(data[k], "encodedslash", "/", -1)
	}
	return
}
