package main

import (
	_ "AllSaintsTest/internal/handlers"
	"AllSaintsTest/pkg/router"
	"flag"
	"github.com/gorilla/handlers"
	"net/http"
	"os"
	"runtime"
)

func main() {
	portPtr := flag.String("port", "8080", "The port to run the server on.")
	flag.Parse()
	go InitialiseHttpListener(portPtr)
	runtime.Goexit()
}

func InitialiseHttpListener(port *string) {
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "origin", "content-type", "Authorization", "authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	http.ListenAndServe(":"+*port, handlers.LoggingHandler(os.Stdout, handlers.CORS(originsOk, headersOk, methodsOk)(router.NewRouter())))
}
