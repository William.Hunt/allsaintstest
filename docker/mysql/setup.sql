create database test_db;
Use test_db;

Create table `tbl_orders`
(
    order_id    int          not NULL AUTO_INCREMENT,
    order_date  datetime     not NULL,
    order_value double       not NULL,
    status      varchar(255) not NULL,
    store_id    int,
    PRIMARY KEY (order_id)
);

Create table `tbl_stores`
(
    store_id   int          not NULL AUTO_INCREMENT,
    store_name varchar(255) not NULL,
    PRIMARY KEY (store_id)
);


insert into tbl_orders (order_date, order_value, status, store_id)
values (NOW(), 35000, 'new', 1),
       (NOW(), 46000, 'cancelled', 1),
       (NOW(), 22000, 'shipped', 2);

insert into tbl_stores (store_name)
values ('Store 1'),
       ('Store 1');

